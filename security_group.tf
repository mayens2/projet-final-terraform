

# Create a Security Group for Load balancer
resource "aws_security_group" "alb" {
  name   = "${var.sg-lb-name}"
  vpc_id = "${aws_vpc.vpc.id}"

  ingress {
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = "80"
    to_port     = "80"
  }


  egress {
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = "0"
    to_port     = "0"
  }

  tags = {
    Name = "${var.sg-lb-name}"
  }
}


# Create a Security Group for master instance

resource "aws_security_group" "instance_master" {
  name   = "${var.sg-master-tag-name}"
  vpc_id = "${aws_vpc.vpc.id}"

  ingress {
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = "22"
    to_port     = "22"
  }


  egress {
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = "0"
    to_port     = "0"
  }

  tags = {
    Name = "${var.sg-master-tag-name}"
  }
}

# Create a Security Group for worker nodes
resource "aws_security_group" "instance_worker_node" {
  name        = var.ec2_name_slave
  vpc_id = "${aws_vpc.vpc.id}"

  ingress {
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    security_groups  = ["${aws_security_group.instance_master.id}"]
  }

  ingress {
      from_port        = var.app_port
      to_port          = var.app_port
      protocol         = "tcp"
      cidr_blocks      = ["0.0.0.0/0"]
    }

  ingress {
    from_port        = 3000
    to_port          = 3000
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]

  }
  tags = {
    Name = "${var.sg-slave-tag-name}"
  }

}
