resource "local_file" "ansible_inventory" {
  content = templatefile("inventory.tmpl", {
    nodes = aws_instance.worker_node.*.private_ip
    key_name=var.key_name
  })
  filename = "ansible/hosts"
}

# Copy ansible folder to master ec2
