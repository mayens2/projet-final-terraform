
resource "aws_instance" "ec2_master" {
  ami			          = var.AMI_ID
  instance_type           = var.instance_type
  key_name = "${aws_key_pair.TF_key_new.key_name}" 
  vpc_security_group_ids  = ["${aws_security_group.instance_master.id}"]
  subnet_id                   = "${aws_subnet.public_subnet[0].id}"  
  associate_public_ip_address = "${var.instance-associate-public-ip}"

  tags = {
    Name = "u-ctrl"
  }
  user_data = <<-EOF
	      #!/bin/bash
	       sudo apt-get update	
         sudo apt-get install -y ansible      
	      EOF			  
  
   connection {
        type     = "ssh"
        user     = "ubuntu"        
        host     = "${aws_instance.ec2_master.public_ip}"
        private_key =file("${local_file.TF_key.filename}")

      }

  provisioner "file" {
      source = "ansible"
      destination = "/home/ubuntu/ansible"
  }

   provisioner "remote-exec" {
    inline = ["sudo chmod 0400 ~/ansible/tfkey.pem"]
  }

}


# Create 3 EC2 instances for worker nodes

resource "aws_instance" "worker_node" {
  ami			          = var.AMI_ID
  count = var.nb_instance
  instance_type           = var.instance_type
  key_name = "${aws_key_pair.TF_key_new.key_name}" 
  vpc_security_group_ids  = ["${aws_security_group.instance_worker_node.id}"]  
  subnet_id            = element(aws_subnet.public_subnet.*.id, count.index)
  associate_public_ip_address = true # TODO: should be false
  

  tags = {
    Name = "u-web-${count.index}"
  }
  user_data = <<-EOF
	      #!/bin/bash
	       sudo apt-get update	                
	      EOF
			  
 
}


####################################################################

# Creating 1 Elastic IPs for master ec2

####################################################################

resource "aws_eip" "eip_master" {  
  instance         = aws_instance.ec2_master.id
  public_ipv4_pool = "amazon"
  vpc              = true

  tags = {
    "Name" = "EIP-master"
  }
}

####################################################################

# Creating EIP association with EC2 Instance Master:

####################################################################

resource "aws_eip_association" "eip_association" {  
  instance_id   = aws_instance.ec2_master.id
  allocation_id = aws_eip.eip_master.id
}

