variable "key_name" {
  type = string
  description = "type instance"
  default = "tfkey.pem"
}

variable "AMI_ID" {
  default = "ami-065deacbcaac64cf2"
}

variable "instance_type" {
  type = string
  description = "type instance"
  default = "t2.micro"
}

variable "app_port" {
  description = "Port used for web app"
  type = string
  default = "8080"
}

variable "nb_instance" {
  description = "Instance number of EC2"
  type = number
  default = 3
}

variable "custom_vpc" {
  description = "VPC for Groupe 2 environment"
  type        = string
  default     = "10.0.0.0/16"
}

variable "availability_zone" {
  type = string
  description = "zone la region"
  default = "eu-central-1a"
}

variable "ec2_name_slave" {
  type = string
  default = "u-web-worker"
}

variable "my_user_data" {
  type = string
  default =  <<-EOF
        #!/bin/bash
        sudo apt-get update
        EOF
}

variable "instance-associate-public-ip" {
  description = "Defines if the EC2 instance has a public IP address."
  type        = string
  default     = "true"
}

variable "vpc-cidr-block" {
  description = "The CIDR block to associate to the VPC"
  type        = string
  default     = "10.0.0.0/16"
}

variable "subnet-cidr-block" {
  description = "The CIDR block to associate to the subnet"
  type        = string
  default     = "10.0.1.0/24"
}

variable "vpc-tag-name" {
  description = "The Name to apply to the VPC"
  type        = string
  default     = "VPC-created-with-terraform"
}

variable "ig-tag-name" {
  description = "The name to apply to the Internet gateway tag"
  type        = string
  default     = "aws-ig-created-with-terraform"
}

variable "subnet-tag-name" {
  description = "The Name to apply to the VPN"
  type        = string
  default     = "Public-Subnet"
}

variable "sg-master-tag-name" {
  description = "SG of Master Node"
  type        = string
  default     = "SG-master-created-with-terraform"
}


variable "sg-lb-name" {
  description = "Security group of Load Balancer"
  type        = string
  default     = "SG-lb-created-with-terraform"
}


variable "sg-slave-tag-name" {
  description = "The Name to apply to the security group for worker nodes"
  type        = string
  default     = "SG-slave-created-with-terraform"
}