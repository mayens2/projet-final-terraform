# create the keypair
resource "tls_private_key" "rsa" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "local_file" "TF_key" {
    content  = tls_private_key.rsa.private_key_pem
    filename = "ansible/${var.key_name}"
}

resource "aws_key_pair" "TF_key_new" {
  key_name   = "TF_key_new"
  public_key = tls_private_key.rsa.public_key_openssh
}

